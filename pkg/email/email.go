package email

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/smtp"
	"text/template"

	"gitlab.com/onework-application/backend/config"
)

type SendEmailRequest struct {
	To      []string
	Type    string
	Body    map[string]string
	Subject string
}

const (
	SignUpEmail       = "sign_up"
	ResetPassworEmail = "reset_password"
)

func SendEmail(ctx context.Context, cfg *config.Config, req *SendEmailRequest) error {
	from := cfg.Smtp.Sender
	to := req.To

	password := cfg.Smtp.Password

	var body bytes.Buffer

	templatePath := getTemplatePath(req.Type)
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		return err
	}
	t.Execute(&body, req.Body)

	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	subject := fmt.Sprintf("Subject: %s\n", req.Subject)
	msg := []byte(subject + mime + body.String())

	auth := smtp.PlainAuth("", from, password, "smtp.gmail.com")
	err = smtp.SendMail("smtp.gmail.com:587", auth, from, to, msg)
	if err != nil {
		return err
	}
	return nil
}

func SendEmailWithAnApi(ctx context.Context, cfg *config.Config, req *SendEmailRequest) error {
	url := "https://api.brevo.com/v3/smtp/email"
	apiKey := "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"

	var body bytes.Buffer

	templatePath := getTemplatePath(req.Type)
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		return err
	}
	t.Execute(&body, req.Body)

	// Create request body
	data := map[string]interface{}{
		"sender": map[string]string{
			"name":  "Onework",
			"email": cfg.Smtp.Sender,
		},
		"to": []map[string]string{
			{
				"email": req.To[0],
			},
		},
		"subject":     req.Subject,
		"htmlContent": body.String(),
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	// Create and send the HTTP request
	request, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Api-Key", apiKey)

	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func getTemplatePath(emailType string) string {
	switch emailType {
	case SignUpEmail:
		return "./templates/sign_up.html"
	case ResetPassworEmail:
		return "./templates/reset_password.html"
	}
	return ""
}
