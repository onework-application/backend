package email

import "strings"

// List of personal email domains
var personalEmailDomains = []string{
	"gmail.com",
	"yahoo.com",
	"outlook.com",
	"icloud.com",
	"aol.com",
	"hotmail.com",
	"live.com",
	"msn.com",
}

func IsPersonalEmail(email string) bool {
	// Extract the domain part of the email
	parts := strings.Split(email, "@")
	if len(parts) != 2 {
		return false
	}

	domain := parts[1]

	// Check if the domain is in the list of personal email domains
	for _, personalDomain := range personalEmailDomains {
		if domain == personalDomain {
			return true
		}
	}

	return false
}
