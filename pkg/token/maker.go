package token

import "context"

// Maker is an interface for managing tokens
type Maker interface {
	// CreateToken created  new token for a specific params and duration
	CreateToken(ctx context.Context, tokenParams *TokenParams) (string, *Payload, error)

	// Verify Token  check id the token is valid or not
	VerifyToken(ctx context.Context, token string) (*Payload, error)
}
