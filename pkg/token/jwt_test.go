package token

import (
	"testing"
	// "time"

	// "github.com/bxcodec/faker/v4"
	// "github.com/golang-jwt/jwt"
	// "github.com/stretchr/testify/require"
	// "gitlab.com/onework-application/backend/pkg/utils"
)

func TestJWTMaker(t *testing.T) {
	// maker, err := NewJWTMaker(utils.RandomString(32))
	// require.NoError(t, err)

	// userID := int64(3)
	// userEmail := faker.Email()
	// duration := time.Minute
	// userType := "applicant"

	// issuedAt := time.Now()
	// experidAt := time.Now().Add(duration)

	// token, payload, err := maker.CreateToken(&TokenParams{
	// 	UserID:   userID,
	// 	Email:    userEmail,
	// 	UserType: userType,
	// 	Duration: duration,
	// })
	// require.NoError(t, err)
	// require.NotEmpty(t, token)
	// require.NotEmpty(t, payload)

	// payload, err = maker.VerifyToken(token)
	// require.NoError(t, err)
	// require.NotEmpty(t, payload)

	// require.NotZero(t, payload.Id)
	// require.Equal(t, userID, payload.UserID)
	// require.Equal(t, userEmail, payload.Email)
	// require.Equal(t, userType, payload.UserType)
	// require.WithinDuration(t, issuedAt, payload.IssuedAt, time.Second)
	// require.WithinDuration(t, experidAt, payload.ExpiredAt, time.Second)
}

func TestExperidJWTToken(t *testing.T) {
	// maker, err := NewJWTMaker(utils.RandomString(32))
	// require.NoError(t, err)

	// userID := int64(3)
	// userEmail := faker.Email()
	// userType := "applicant"

	// token, payload, err := maker.CreateToken(&TokenParams{
	// 	UserID:   userID,
	// 	Email:    userEmail,
	// 	UserType: userType,
	// 	Duration: -time.Minute,
	// })
	// require.NoError(t, err)
	// require.NotEmpty(t, token)
	// require.NotEmpty(t, payload)

	// payload, err = maker.VerifyToken(token)
	// require.Error(t, err)
	// require.EqualError(t, err, ErrExpiredToken.Error())
	// require.Nil(t, payload)
}

func TestInvalidJWTTokenAlgNone(t *testing.T) {
	// userID := int64(3)
	// userEmail := faker.Email()
	// userType := "applicant"

	// payload, err := NewPayload(&TokenParams{
	// 	UserID:   userID,
	// 	Email:    userEmail,
	// 	UserType: userType,
	// 	Duration: time.Minute,
	// })
	// require.NoError(t, err)

	// jwtToken := jwt.NewWithClaims(jwt.SigningMethodNone, payload)
	// token, err := jwtToken.SignedString(jwt.UnsafeAllowNoneSignatureType)
	// require.NoError(t, err)

	// maker, err := NewJWTMaker(utils.RandomString(32))
	// require.NoError(t, err)

	// payload, err = maker.VerifyToken(token)
	// require.Error(t, err)
	// require.EqualError(t, err, ErrInvalidToken.Error())
	// require.Nil(t, payload)
}
