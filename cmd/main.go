package main

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"gitlab.com/onework-application/backend/api"
	_ "gitlab.com/onework-application/backend/api/docs"
	"gitlab.com/onework-application/backend/config"
	"gitlab.com/onework-application/backend/pkg/jaeger"
	"gitlab.com/onework-application/backend/pkg/logger"
	"gitlab.com/onework-application/backend/pkg/token"
	"gitlab.com/onework-application/backend/storage"
	"go.opentelemetry.io/otel"
)

func main() {
	cfg := config.Load(".")

	logger.Init()
	log := logger.GetLogger()

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)

	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.RedisAddr,
	})

	tp, err := jaeger.InitJaeger(&cfg)
	if err != nil {
		log.Fatal("cannot create tracer", err)
	}
	defer tp.Shutdown(context.Background())

	otel.SetTracerProvider(tp)
	tr := tp.Tracer("main-otel")
	log.Info("Opentracing connected")

	strg := storage.NewStoragePg(psqlConn, tr)
	inMemory := storage.NewInMemoryStorage(rdb)

	maker, err := token.NewJWTMaker(cfg.TokenSymmetricKey)
	if err != nil {
		log.Fatalf("failed to get new paseto maker %v", err)
	}

	server := api.New(&api.RoutetOptions{
		Cfg:      &cfg,
		Storage:  strg,
		InMemory: inMemory,
		Tracer:   tr,
		Log:      log,
		Maker:    maker,
	})

	if err := server.Run(cfg.HttpPort); err != nil {
		log.Fatalf("failed to run HTTP server: %v", err)
	}
}
