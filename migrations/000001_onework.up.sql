CREATE TABLE IF NOT EXISTS "users" (
    "id" VARCHAR PRIMARY KEY, -- IT SHOULD GET UUID UNIQUE
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "email" VARCHAR NOT NULL, -- corparative email or personal
    "password" VARCHAR,  -- hashed password
    "user_type" VARCHAR NOT NULL, -- applicant, employer -> (employer, employer_admin), admin
    "created_at" TIMESTAMPTZ DEFAULT timezone('Asia/Tashkent', CURRENT_TIMESTAMP) NOT NULL
)