package storage

import (
	"context"
	"encoding/json"
	"time"

	"github.com/redis/go-redis/v9"
)

type InMemoryStorageI interface {
	Set(ctx context.Context, key string, value map[string]interface{}, exp time.Duration) error
	Get(ctx context.Context, key string) (map[string]interface{}, error)
}

type storageRedis struct {
	client *redis.Client
}

func NewInMemoryStorage(rdb *redis.Client) InMemoryStorageI {
	return &storageRedis{
		client: rdb,
	}
}

func (rd *storageRedis) Set(ctx context.Context, key string, value map[string]interface{}, exp time.Duration) error {
	// Marshal the map into a JSON string
	jsonBytes, err := json.Marshal(value)
	if err != nil {
		return err
	}

	err = rd.client.Set(context.Background(), key, string(jsonBytes), exp).Err()
	if err != nil {
		return err
	}
	return nil
}

func (rd *storageRedis) Get(ctx context.Context, key string) (map[string]interface{}, error) {
	val, err := rd.client.Get(context.Background(), key).Result()
	if err != nil {
		return nil, err
	}

	var res map[string]interface{}
	// Marshal the map into a JSON string
	err = json.Unmarshal([]byte(val), &res)
	if err != nil {
		return nil, err
	}
	return res, nil
}
