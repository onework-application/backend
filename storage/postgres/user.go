package postgres

import (
	"context"
	"database/sql"
	"errors"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-application/backend/storage/repo"
	"go.opentelemetry.io/otel/trace"
)

type userRepo struct {
	db     *sqlx.DB
	tracer trace.Tracer
}

func NewUser(db *sqlx.DB, tracer trace.Tracer) repo.UserStorageI {
	return &userRepo{
		db:     db,
		tracer: tracer,
	}
}

// create user after successfully sign up and verify email
func (u *userRepo) Create(ctx context.Context, user *repo.User) (*repo.User, error) {
	_, tr := u.tracer.Start(ctx, "user.Create")
	defer tr.End()

	query := `
		INSERT INTO users (id,first_name, last_name, email, password, user_type) VALUES ($1, $2, $3, $4, $5, $6) RETURNING created_at
	`

	id := uuid.New()

	err := u.db.QueryRow(query, id.String(), user.FirstName, user.LastName, user.Email, user.Password, user.UserType).Scan(&user.CreatedAt)

	user.ID = id.String()

	return user, err
}

func (u *userRepo) Get(ctx context.Context, userID string) (*repo.User, error) {
	_, tr := u.tracer.Start(ctx, "user.Get")
	defer tr.End()

	query := `
		SELECT 
			id, email, password, user_type, created_at 
		FROM users WHERE id = $1
	`

	var (
		user repo.User
	)
	err := u.db.QueryRow(query, userID).Scan(
		&user.ID,
		&user.Email,
		&user.Password,
		&user.UserType,
		&user.CreatedAt,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}

	return &user, nil
}

func (u *userRepo) GetWithEmail(ctx context.Context, email string) (*repo.User, error) {
	_, tr := u.tracer.Start(ctx, "user.Get")
	defer tr.End()

	query := `
		SELECT 
			id, email, password, user_type, created_at, first_name, last_name
		FROM users WHERE email = $1
	`

	var (
		user repo.User
	)
	err := u.db.QueryRow(query, email).Scan(
		&user.ID,
		&user.Email,
		&user.Password,
		&user.UserType,
		&user.CreatedAt,
		&user.FirstName,
		&user.LastName,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, sql.ErrNoRows
		}
		return nil, err
	}

	return &user, nil
}

func (u *userRepo) UpdatePasswordWithEmail(ctx context.Context, email, password string) error {
	_, tr := u.tracer.Start(ctx, "user.UpdatePasswordWithEmail")
	defer tr.End()

	query := `
		UPDATE users SET password = $1 WHERE email = $2 RETURNING id
	`

	_, err := u.db.Exec(query, password, email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return sql.ErrNoRows
		}
		return err
	}

	return nil
}
