package repo

import (
	"context"
	"time"
)

type User struct {
	ID        string
	FirstName string
	LastName  string
	Email     string
	Password  string
	UserType  string
	CreatedAt time.Time
}

type UserStorageI interface {
	Create(ctx context.Context, user *User) (*User, error)
	Get(ctx context.Context, userID string) (*User, error)
	GetWithEmail(ctx context.Context, email string) (*User, error)
	UpdatePasswordWithEmail(ctx context.Context, email, password string) error
}
