package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-application/backend/storage/postgres"
	"gitlab.com/onework-application/backend/storage/repo"
	"go.opentelemetry.io/otel/trace"
)

type StorageI interface {
	User() repo.UserStorageI
}

type StoragePg struct {
	userRepo repo.UserStorageI
}

func NewStoragePg(db *sqlx.DB, tracer trace.Tracer) StorageI {
	return &StoragePg{
		userRepo: postgres.NewUser(db, tracer),
	}
}

func (s *StoragePg) User() repo.UserStorageI {
	return s.userRepo
}
