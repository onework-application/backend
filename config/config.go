package config

import (
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	HttpPort                   string
	RedisAddr                  string
	TokenSymmetricKey          string
	FrontendUrl                string
	AccessTokenDuration        time.Duration
	ResetPasswordTokenDuration time.Duration
	Postgres                   PostgresConfig
	Jaeger                     Jaeger
	Smtp                       Smtp
	AuthorizationHeaderKey     string
	AuthorizationPayloadKey    string
}

type Smtp struct {
	Sender   string
	Password string
}

type Jaeger struct {
	Endpoint    string
	ServiceName string
}

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	return Config{
		HttpPort:    conf.GetString("HTTP_PORT"),
		RedisAddr:   conf.GetString("REDIS_ADDR"),
		FrontendUrl: conf.GetString("FRONTEND_URL"),
		Postgres: PostgresConfig{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},
		Jaeger: Jaeger{
			Endpoint:    conf.GetString("JAEGER_ENDPOINT"),
			ServiceName: conf.GetString("JAEGER_SERVICE_NAME"),
		},
		Smtp: Smtp{
			Sender:   conf.GetString("SMTP_SENDER"),
			Password: conf.GetString("SMTP_PASSWORD"),
		},
		TokenSymmetricKey:          conf.GetString("TOKEN_SYMMETRIC_KEY"),
		AccessTokenDuration:        conf.GetDuration("ACCESS_TOKEN_DURATION"),
		ResetPasswordTokenDuration: conf.GetDuration("RESET_PASSWORD_TOKEN_DURATION"),
		AuthorizationHeaderKey:     conf.GetString("AUTHORIZATION_HEADER_KEY"),
		AuthorizationPayloadKey:    conf.GetString("AUTHORIZATION_PAYLOAD_KEY"),
	}
}
