-include .env
.SILENT:
CURRENT_DIR=$(shell pwd)
DB_URL=postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)?sslmode=disable

build:
	@go build -o bin/server ./cmd/main.go

run: build
	@./bin/server

tidy:
	go mod tidy
	go mod vendor

local:
	docker compose -f docker-compose.yml up --build -d

down:
	docker compose down

migrateup:
	migrate -path migrations -database "$(DB_URL)" -verbose up

migrateup1:
	migrate -path migrations -database "$(DB_URL)" -verbose up 1

migratedown:
	migrate -path migrations -database "$(DB_URL)" -verbose down

migratedown1:
	migrate -path migrations -database "$(DB_URL)" -verbose down 1

create-migration:
	migrate create -ext sql -dir migrations -seq $(name)

unit-test:
	go test -v -cover ./storage/postgres/...
lint:
	golangci-lint run ./...

cache:
	go clean -testcache

swag-init:
	swag init -g api/api.go -o api/docs
