package v1

import (
	"context"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-application/backend/api/models"
	"gitlab.com/onework-application/backend/pkg/token"
)

func (h *handlerV1) AuthMiddleWare(ctx *gin.Context) {
	accessToken := ctx.GetHeader(h.cfg.AuthorizationHeaderKey)

	if len(accessToken) == 0 {
		err := errors.New("authorization header is not provided")
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.ResponseError{
			Error:       err.Error(),
			Description: "Set token to make a request on this api!",
		})
		return
	}
	payload, err := h.maker.VerifyToken(context.Background(), accessToken)

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, models.ResponseError{
			Error:       err.Error(),
			Description: "It looks like token is invalid!",
		})
		return
	}

	ctx.Set(h.cfg.AuthorizationPayloadKey, payload)
	ctx.Next()
}

func (h *handlerV1) GetAuthPayload(ctx *gin.Context) (*token.Payload, error) {
	i, exist := ctx.Get(h.cfg.AuthorizationPayloadKey)
	if !exist {
		return nil, errors.New("not found payload")
	}

	payload, ok := i.(*token.Payload)
	if !ok {
		return nil, errors.New("unknown user")
	}
	return payload, nil
}
