package v1

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/onework-application/backend/api/models"
	"gitlab.com/onework-application/backend/pkg/email"
	"gitlab.com/onework-application/backend/pkg/token"
	"gitlab.com/onework-application/backend/pkg/utils"
	"gitlab.com/onework-application/backend/storage/repo"
)

// @Summary Sign up
// @Description Use this api in order to sign up for the type of employee and employer into the system. Api will send 6 digit code to provided email! Types = {employee, employer}
// @Tags register
// @Accept json
// @Produce json
// @Param data body models.SignupReq true "Data"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/sign-up [post]
func (h *handlerV1) SignUp(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.CreatePassword")
	defer tr.End()

	var req models.SignupReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If provided required fields but still you are getting error, you may not passing valid email.",
		})
		return
	}

	// check type is one of employee and employer
	if !(req.Type == models.EmployeeType || req.Type == models.EmployerType) {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Type is invalid",
			Description: "User type should be 'employer' or 'employee'!",
		})
		return
	}

	// check email is corparative
	if req.Type == models.EmployerType {
		if email.IsPersonalEmail(req.Email) {
			ctx.JSON(http.StatusBadRequest, models.ResponseError{
				Error:       "Provided email is not corparative. Please, provide your company email which is like 'username@domain.uz'",
				Description: "not_corparative",
			})
			return
		}
	}

	user, err := h.Storage.User().GetWithEmail(c, req.Email)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Bad request",
		})
		return
	}
	if user != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Email exist!",
			Description: user.UserType,
		})
		return
	}

	// generate 6 digit code
	code, err := utils.GenerateRandomCode(6)
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Please try again, in order to get verification code!",
		})
		return
	}

	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, Please try again!",
		})
		return
	}

	go func() {
		// set code to redis, after 6 minute code will be invalid
		err = h.inMemory.Set(c, req.Email, map[string]interface{}{
			"code":       code,
			"email":      req.Email,
			"type":       req.Type,
			"first_name": req.FirstName,
			"last_name":  req.LastName,
			"password":   hashedPassword,
		}, time.Minute*10)
		if err != nil {
			h.log.Error(err)
		}
	}()

	// send verification code to provided email address
	if err := email.SendEmailWithAnApi(c, h.cfg, &email.SendEmailRequest{
		To:   []string{req.Email},
		Type: "sign_up",
		Body: map[string]string{
			"code": code,
		},
		Subject: "Verification Code",
	}); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: fmt.Sprintf("Failed to send verification code to %v email. Please, provide valid email address!", req.Email),
		})
		return
	}

	// return success response
	ctx.JSON(http.StatusOK, models.ResponseOk{
		Response: fmt.Sprintf("Verification code sent to %v. Please check your inbox! If you did not get email, check your spam folder.", req.Email),
	})
}

// @Summary Verify Email
// @Description Verify email with 6 digit code after succesfully sign-up
// @Tags register
// @Accept json
// @Produce json
// @Param data body models.SignUpVerifyReq true "Data"
// @Success 200 {object} models.VerifyEmailRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/verify [post]
func (h *handlerV1) VerifySignUp(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.CreatePassword")
	defer tr.End()

	var req models.SignUpVerifyReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If provided required fields but still you are getting error, you may not passing valid email.",
		})
		return
	}

	// get from redis with email
	res, err := h.inMemory.Get(c, req.Email)
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}

	code, ok := res["code"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}
	email, ok := res["email"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}
	userType, ok := res["type"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}
	password, ok := res["password"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}
	firstName, ok := res["first_name"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}
	lastName, ok := res["last_name"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}

	// check code is not wrong
	if req.Code != code.(string) {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because wrong!",
			Description: fmt.Sprintf("Verification code for the %v email address is completely wrong!", email),
		})
		return
	}

	// create user after all things successfully done
	response, err := h.Storage.User().Create(c, &repo.User{
		Email:     email.(string),
		UserType:  userType.(string),
		FirstName: firstName.(string),
		LastName:  lastName.(string),
		Password:  password.(string),
	})
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "While creating user, got unexpected error",
		})
		return
	}

	// create token
	token, payload, err := h.maker.CreateToken(c, &token.TokenParams{
		UserID:   response.ID,
		Email:    response.Email,
		Duration: h.cfg.AccessTokenDuration,
	})
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "While creating token, got unexpected error",
		})
		return
	}

	// make a response to request
	ctx.JSON(http.StatusOK, models.VerifyEmailRes{
		Token: struct {
			ID        string    "json:\"token_id\""
			Token     string    "json:\"token\""
			IssuedAt  time.Time "json:\"issued_at\""
			ExpiresAt time.Time "json:\"expires_at\""
		}{
			ID:        payload.Id.String(),
			Token:     token,
			IssuedAt:  payload.IssuedAt,
			ExpiresAt: payload.ExpiredAt,
		},
		Email:     response.Email,
		ID:        response.ID,
		Type:      response.UserType,
		FirstName: response.FirstName,
		LastName:  response.LastName,
		CreatedAt: response.CreatedAt,
	})
}

// @Security ApiKeyAuth
// @Summary Create password
// @Description Create password after successfully sign up to system as employee or employer
// @Tags register
// @Accept json
// @Produce json
// @Param data body models.PasswordReq true "Data"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/password [post]
// func (h *handlerV1) CreatePassword(ctx *gin.Context) {
// 	c, tr := h.tracer.Start(ctx, "auth.CreatePassword")
// 	defer tr.End()

// 	var req models.PasswordReq
// 	if err := ctx.ShouldBindJSON(&req); err != nil {
// 		h.log.Error(err)
// 		ctx.JSON(http.StatusBadRequest, models.ResponseError{
// 			Error:       err.Error(),
// 			Description: "Provide all required fields! If you provided required fields but still you are getting error, you may not enter max password length 8",
// 		})
// 		return
// 	}

// 	res, err := h.GetAuthPayload(ctx)
// 	if err != nil {
// 		h.log.Error(err)
// 		ctx.JSON(http.StatusBadRequest, models.ResponseError{
// 			Error:       err.Error(),
// 			Description: "Token is not valid or not provided!",
// 		})
// 		return
// 	}

// 	user, err := h.Storage.User().Get(c, res.UserID)
// 	if err != nil {
// 		h.log.Error(err)
// 		if errors.Is(err, sql.ErrNoRows) {
// 			ctx.JSON(http.StatusBadRequest, models.ResponseError{
// 				Error:       err.Error(),
// 				Description: "User with an id does not exist",
// 			})
// 			return
// 		}
// 		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
// 			Error:       err.Error(),
// 			Description: "Something went wrong, Please try again!",
// 		})
// 		return
// 	}

// 	if user.Password != "" {
// 		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
// 			Error:       "Password already exist",
// 			Description: "You can not create password again and again after sign up. You may update your password with an update password api!",
// 		})
// 		return
// 	}

// 	hashedPassword, err := utils.HashPassword(req.Password)
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
// 			Error:       err.Error(),
// 			Description: "Something went wrong, Please try again!",
// 		})
// 		return
// 	}

// 	err = h.Storage.User().UpdatePassword(c, res.UserID, hashedPassword)
// 	if err != nil {
// 		h.log.Error(err)
// 		if errors.Is(err, sql.ErrNoRows) {
// 			ctx.JSON(http.StatusBadRequest, models.ResponseError{
// 				Error:       err.Error(),
// 				Description: "User with an id does not exist",
// 			})
// 			return
// 		}
// 		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
// 			Error:       err.Error(),
// 			Description: "Something went wrong, Please try again!",
// 		})
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, models.ResponseOk{
// 		Response: "Successfully password created!",
// 	})
// }

// @Summary Login
// @Description Login employer and employee
// @Tags register
// @Accept json
// @Produce json
// @Param data body models.LoginReq true "Data"
// @Success 200 {object} models.VerifyEmailRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/login [post]
func (h *handlerV1) Login(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.Login")
	defer tr.End()

	var req models.LoginReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If you provided required fields but still you are getting error, you may not enter valid email",
		})
		return
	}

	user, err := h.Storage.User().GetWithEmail(c, req.Email)
	if err != nil {
		h.log.Error(err)
		if errors.Is(err, sql.ErrNoRows) {
			ctx.JSON(http.StatusBadRequest, models.ResponseError{
				Error:       err.Error(),
				Description: "Email does not exist.",
			})
			return
		}
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, try again!",
		})
		return
	}

	if err := utils.CheckPassword(req.Password, user.Password); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Password or Email is not true",
			Description: "Please, check your email and password!",
		})
		return
	}

	// create token
	token, payload, err := h.maker.CreateToken(c, &token.TokenParams{
		UserID:   user.ID,
		Email:    user.Email,
		Duration: h.cfg.AccessTokenDuration,
	})
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "While creating token, got unexpected error",
		})
		return
	}

	// make a response to request
	ctx.JSON(http.StatusOK, models.VerifyEmailRes{
		Token: struct {
			ID        string    "json:\"token_id\""
			Token     string    "json:\"token\""
			IssuedAt  time.Time "json:\"issued_at\""
			ExpiresAt time.Time "json:\"expires_at\""
		}{
			ID:        payload.Id.String(),
			Token:     token,
			IssuedAt:  payload.IssuedAt,
			ExpiresAt: payload.ExpiredAt,
		},
		Email:     user.Email,
		ID:        user.ID,
		Type:      user.UserType,
		CreatedAt: user.CreatedAt,
	})
}

// @Summary Email exits
// @Description Check email is exists
// @Tags email
// @Accept json
// @Produce json
// @Param data body models.IsEmailExistReq true "Data"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /email/exist [post]
func (h *handlerV1) IsEmailExits(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.IsEmailExits")
	defer tr.End()

	var req models.IsEmailExistReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If you provided required fields but still you are getting error, you may not enter valid email",
		})
		return
	}

	_, err := h.Storage.User().GetWithEmail(c, req.Email)
	if err != nil {
		h.log.Error(err)
		if errors.Is(err, sql.ErrNoRows) {
			ctx.JSON(http.StatusBadRequest, models.ResponseError{
				Error:       err.Error(),
				Description: "Email does not exist.",
			})
			return
		}
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, try again!",
		})
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOk{
		Response: "Email exist",
	})
}

// @Summary Reset Password To Send Code To Email
// @Description Enter an email to get a code to the email of user in order to reset password. Before making a request to this api. Firt of all make a request to v1/email/exist, in order to check email exist!
// @Tags password
// @Accept json
// @Produce json
// @Param data body models.IsEmailExistReq true "Data"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/password/reset [post]
func (h *handlerV1) ResetPassword(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.ResetPassword")
	defer tr.End()

	var req models.IsEmailExistReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If you provided required fields but still you are getting error, you may not enter valid email",
		})
		return
	}

	user, err := h.Storage.User().GetWithEmail(c, req.Email)
	if err != nil {
		h.log.Error(err)
		if errors.Is(err, sql.ErrNoRows) {
			ctx.JSON(http.StatusBadRequest, models.ResponseError{
				Error:       err.Error(),
				Description: "Email does not exist.",
			})
			return
		}
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, try again!",
		})
		return
	}

	code, err := utils.GenerateRandomCode(6)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, try again!",
		})
		return
	}

	// set code to redis, after 6 minute code will be invalid
	err = h.inMemory.Set(c, req.Email, map[string]interface{}{
		"code":  code,
		"email": req.Email,
	}, time.Minute*10)
	if err != nil {
		h.log.Error(err)
	}

	// Get the user agent from the request headers
	userAgent := ctx.GetHeader("User-Agent")
	application, os, what := parseUserAgent(userAgent)

	// Get the client IP address from the request
	ip := ctx.Copy().ClientIP()

	location, err := getLocation(ip)
	if err != nil {
		h.log.Error(err.Error())
	}

	/// Get the client's time zone from the request headers
	timezone := ctx.Request.Header.Get("Timezone")

	// Set the time zone for parsing and formatting
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		log.Println("Failed to load time zone:", err)
		return
	}

	// Get the current time in the client's time zone
	clientTime := time.Now().In(loc)
	log.Println(clientTime)

	// Format the time as desired
	formattedTime := clientTime.Format("Jan 02, 2006 03:04 PM MST")

	// send reset passwordlink to email
	if err := email.SendEmailWithAnApi(c, h.cfg, &email.SendEmailRequest{
		To:   []string{req.Email},
		Type: "reset_password",
		Body: map[string]string{
			"code":   code,
			"name":   user.FirstName,
			"when":   formattedTime,
			"device": fmt.Sprintf("%v %v %v", what, os, application),
			"near":   fmt.Sprintf("%v, %v", location.City, location.Country),
		},
		Subject: "Reset password",
	}); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: fmt.Sprintf("Failed to send reset password code to %v email. Please, provide valid email address!", req.Email),
		})
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOk{
		Response: fmt.Sprintf("Successfully reset password link sent to %v email address", user.Email),
	})
}

// @Summary Verify Reset Password with enteting Code
// @Description Provide an email and a code to verify in order to reset password
// @Tags password
// @Accept json
// @Produce json
// @Param data body models.SignUpVerifyReq true "Data"
// @Success 200 {object} models.VerifyResetPasswordRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/password/verify [post]
func (h *handlerV1) VerifyResetPassword(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.VerifyResetPassword")
	defer tr.End()

	var req models.SignUpVerifyReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If you provided required fields but still you are getting error, you may not enter valid email",
		})
		return
	}

	// get from redis with email
	res, err := h.inMemory.Get(c, req.Email)
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}

	code, ok := res["code"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}
	email, ok := res["email"]
	if !ok {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is invalid because expired!",
			Description: "Verification code already expired. Please Sign up, again!",
		})
		return
	}

	if req.Code != code.(string) {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is wrong!",
			Description: fmt.Sprintf("Verification code for the %v email address is completely wrong!", email),
		})
		return
	}

	res2, err := h.Storage.User().GetWithEmail(c, req.Email)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       "Verification code is wrong!",
			Description: fmt.Sprintf("Verification code for the %v email address is completely wrong!", email),
		})
		return
	}

	// create token
	token, _, err := h.maker.CreateToken(c, &token.TokenParams{
		UserID:   res2.ID,
		Email:    req.Email,
		Duration: h.cfg.ResetPasswordTokenDuration,
	})
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "While creating token, got unexpected error",
		})
		return
	}

	ctx.JSON(http.StatusOK, models.VerifyResetPasswordRes{
		AccessToken: token,
	})
}

// @Security ApiKeyAuth
// @Summary Update password
// @Description You can update password after verify that the account is yours with Access Token
// @Tags password
// @Accept json
// @Produce json
// @Param data body models.PasswordReq true "Data"
// @Success 200 {object} models.VerifyResetPasswordRes
// @Failure 500 {object} models.ResponseError
// @Failure 400 {object} models.ResponseError
// @Router /auth/password [put]
func (h *handlerV1) UpdatePassword(ctx *gin.Context) {
	c, tr := h.tracer.Start(ctx, "auth.CreatePassword")
	defer tr.End()

	var req models.PasswordReq
	if err := ctx.ShouldBindJSON(&req); err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Provide all required fields! If you provided required fields but still you are getting error, you may not enter max password length 8",
		})
		return
	}

	res, err := h.GetAuthPayload(ctx)
	if err != nil {
		h.log.Error(err)
		ctx.JSON(http.StatusBadRequest, models.ResponseError{
			Error:       err.Error(),
			Description: "Token is not valid or not provided!",
		})
		return
	}

	user, err := h.Storage.User().Get(c, res.UserID)
	if err != nil {
		h.log.Error(err)
		if errors.Is(err, sql.ErrNoRows) {
			ctx.JSON(http.StatusBadRequest, models.ResponseError{
				Error:       err.Error(),
				Description: "User with an id does not exist",
			})
			return
		}
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, Please try again!",
		})
		return
	}

	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, Please try again!",
		})
		return
	}

	err = h.Storage.User().UpdatePasswordWithEmail(c, user.Email, hashedPassword)
	if err != nil {
		h.log.Error(err)
		if errors.Is(err, sql.ErrNoRows) {
			ctx.JSON(http.StatusBadRequest, models.ResponseError{
				Error:       err.Error(),
				Description: "User with an id does not exist",
			})
			return
		}
		ctx.JSON(http.StatusInternalServerError, models.ResponseError{
			Error:       err.Error(),
			Description: "Something went wrong, Please try again!",
		})
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOk{
		Response: "Successfully password updated. Now, you can login with your new password.",
	})
}
