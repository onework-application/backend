package v1

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitlab.com/onework-application/backend/config"
	"gitlab.com/onework-application/backend/pkg/logger"
	"gitlab.com/onework-application/backend/pkg/token"
	"gitlab.com/onework-application/backend/storage"
	"go.opentelemetry.io/otel/trace"
)

type handlerV1 struct {
	cfg      *config.Config
	Storage  storage.StorageI
	inMemory storage.InMemoryStorageI
	tracer   trace.Tracer
	log      logger.Logger
	maker    token.Maker
}

type HandlerV1Options struct {
	Cfg      *config.Config
	Storage  *storage.StorageI
	InMemory *storage.InMemoryStorageI
	Tracer   trace.Tracer
	Log      logger.Logger
	Maker    token.Maker
}

func New(options *HandlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:      options.Cfg,
		Storage:  *options.Storage,
		inMemory: *options.InMemory,
		tracer:   options.Tracer,
		log:      options.Log,
		maker:    options.Maker,
	}
}

func parseUserAgent(userAgent string) (string, string, string) {
	// Check for common patterns in User-Agent string to identify application and OS
	application, os, what := "", "", ""
	if strings.Contains(userAgent, "Postman") {
		application, os = "(Postman)", "Unknown"
	} else if strings.Contains(userAgent, "curl") {
		application, os = "(cURL)", "Unknown"
	} else if strings.Contains(userAgent, "Android") {
		application, os = "(Mobile)", "Android"
	} else if strings.Contains(userAgent, "iPhone") {
		application, os = "(Mobile)", "iOS"
	} else if strings.Contains(userAgent, "Windows") {
		application, os = "(Desktop)", "Windows"
	} else if strings.Contains(userAgent, "Macintosh") {
		application, os = "(Desktop)", "macOS"
	} else {
		application, os = "Unknown", "Unknown"
	}

	// Check for specific keywords to identify the browser
	if strings.Contains(userAgent, "Chrome") {
		what = "Google Chrome"
	} else if strings.Contains(userAgent, "Firefox") {
		what = "Mozilla Firefox"
	} else if strings.Contains(userAgent, "Safari") {
		what = "Apple Safari"
	} else if strings.Contains(userAgent, "Opera") {
		what = "Opera"
	} else if strings.Contains(userAgent, "Edge") {
		what = "Microsoft Edge"
	} else if strings.Contains(userAgent, "Trident") {
		what = "Microsoft Trident"
	} else if strings.Contains(userAgent, "MSIE") {
		what = "Microsoft MSIE"
	} else if strings.Contains(userAgent, "Windows NT") {
		what = "Microsoft Windows NT"
	} else {
		what = "Unknown"
	}

	return application, os, what
}

type IPAddressInfo struct {
	Country string `json:"country"`
	City    string `json:"city"`
}

func getLocation(ip string) (*IPAddressInfo, error) {
	log.Println(ip)
	apiURL := fmt.Sprintf("http://ip-api.com/json/%s", ip)

	response, err := http.Get(apiURL)
	if err != nil {
		fmt.Println("Error making the API request:", err)
		return nil, err
	}
	defer response.Body.Close()

	var ipAddressInfo IPAddressInfo
	err = json.NewDecoder(response.Body).Decode(&ipAddressInfo)
	if err != nil {
		fmt.Println("Error decoding the API response:", err)
		return nil, err
	}

	if ipAddressInfo.City == "" {
		ipAddressInfo.City = "localhost"
	}
	if ipAddressInfo.Country == "" {
		ipAddressInfo.Country = "localhost"
	}

	return &ipAddressInfo, nil
}
