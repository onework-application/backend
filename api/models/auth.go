package models

import "time"

type SignupReq struct {
	FirstName string `json:"first_name" binding:"required"`
	LastName  string `json:"last_name" binding:"required"`
	Email     string `json:"email" binding:"required,email"`
	Password  string `json:"password" binding:"required,min=8"`
	Type      string `json:"type" binding:"required"`
}

type SignUpVerifyReq struct {
	Email string `json:"email" binding:"required,email"`
	Code  string `json:"code" binding:"required,min=6,max=6"`
}

type VerifyEmailRes struct {
	Token struct {
		ID        string    `json:"token_id"`
		Token     string    `json:"token"`
		IssuedAt  time.Time `json:"issued_at"`
		ExpiresAt time.Time `json:"expires_at"`
	} `json:"token_info"`
	Email     string    `json:"email"`
	ID        string    `json:"user_id"`
	Type      string    `json:"type"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	CreatedAt time.Time `json:"created_at"`
}

type PasswordReq struct {
	Password string `json:"password" binding:"required,min=8"`
}

type LoginReq struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

type IsEmailExistReq struct {
	Email string `json:"email" binding:"required,email"`
}

type VerifyResetPasswordRes struct {
	AccessToken string `json:"access_token"`
}
