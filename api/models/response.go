package models

type ResponseOk struct {
	Response string `json:"response"`
}

type ResponseError struct {
	Error       string `json:"error"`
	Description string `json:"description"`
}
