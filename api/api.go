package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	v1 "gitlab.com/onework-application/backend/api/v1"
	"gitlab.com/onework-application/backend/config"
	"gitlab.com/onework-application/backend/pkg/logger"
	"gitlab.com/onework-application/backend/pkg/token"
	"gitlab.com/onework-application/backend/storage"
	"go.opentelemetry.io/otel/trace"
)

type RoutetOptions struct {
	Cfg      *config.Config
	Storage  storage.StorageI
	InMemory storage.InMemoryStorageI
	Tracer   trace.Tracer
	Log      logger.Logger
	Maker    token.Maker
}

// New @title       Swagger for onework api
// @version         2.0
// @description     This is a onework api.
// @BasePath  		/v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @Security ApiKeyAuth
// @Host localhost:2222
func New(opt *RoutetOptions) *gin.Engine {
	router := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowHeaders = append(corsConfig.AllowHeaders, "*")
	router.Use(cors.New(corsConfig))

	handlerV1 := v1.New(&v1.HandlerV1Options{
		Cfg:      opt.Cfg,
		Storage:  &opt.Storage,
		InMemory: &opt.InMemory,
		Tracer:   opt.Tracer,
		Log:      opt.Log,
		Maker:    opt.Maker,
	})

	apiV1 := router.Group("/v1")
	// router.Static("/media", "./medias")

	// Sign up and Login and Verify
	apiV1.POST("/auth/sign-up", handlerV1.SignUp)
	apiV1.POST("/auth/verify", handlerV1.VerifySignUp)
	apiV1.POST("/auth/login", handlerV1.Login)

	// Forgot Password
	apiV1.POST("/auth/password/reset", handlerV1.ResetPassword)
	apiV1.POST("/auth/password/verify", handlerV1.VerifyResetPassword)
	apiV1.PUT("/auth/password", handlerV1.AuthMiddleWare, handlerV1.UpdatePassword)

	// Email exists
	apiV1.POST("/email/exist", handlerV1.IsEmailExits)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
